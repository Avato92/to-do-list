import firebase from 'firebase/app';
import 'firebase/firestore';

const firebaseConfig = firebase.initializeApp({
  apiKey: 'AIzaSyCDurZRBovk8b4Z2dvw5ofJag_K2GzJYOc',
  authDomain: 'to-do-list-751e2.firebaseapp.com',
  databaseURL:
    'https://to-do-list-751e2-default-rtdb.europe-west1.firebasedatabase.app',
  projectId: 'to-do-list-751e2',
  storageBucket: 'to-do-list-751e2.appspot.com',
  messagingSenderId: '783987563105',
  appId: '1:783987563105:web:4bce9e30519609fc0fa820',
});

export { firebaseConfig as firebase };
